package laboratorio;
import java.util.Scanner;

public class Trabajo4 {
	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        System.out.print("Por favor, ingresa la velocidad en Km/h: ");
	        double velocidadKmh = scanner.nextDouble();
	        double velocidadMs = (velocidadKmh * 1000) / 3600;
	        System.out.println("La velocidad " + velocidadKmh + " Km/h equivale a " + velocidadMs + " m/s.");
	    }

}
