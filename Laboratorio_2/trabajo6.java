package laboratorio;
import java.util.Scanner;
public class trabajo6 {

	 public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);

	        System.out.println("Calculadora de Área de Triángulo");
	        System.out.print("Ingresa la longitud del lado a: ");
	        double ladoA = scanner.nextDouble();

	        System.out.print("Ingresa la longitud del lado b: ");
	        double ladoB = scanner.nextDouble();

	        System.out.print("Ingresa la longitud del lado c: ");
	        double ladoC = scanner.nextDouble();

	        double semiperimetro = (ladoA + ladoB + ladoC) / 2;
	        double area = Math.sqrt(semiperimetro * (semiperimetro - ladoA) * (semiperimetro - ladoB) * (semiperimetro - ladoC));

	        System.out.println("El área del triángulo es: " + area);


	    }

}
