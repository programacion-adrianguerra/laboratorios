package laboratorio;
import java.util.Scanner;
public class trabajo7 {

	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Calculadora de Volumen de Esfera");
        System.out.print("Ingresa la longitud del radio de la esfera: ");
        double radio = scanner.nextDouble();
        double volumen = (4.0 / 3.0) * Math.PI * Math.pow(radio, 3);

        System.out.println("El volumen de la esfera es: " + volumen);
    }
}
