public class L13_113 {
        public static void main(String[] args) {
            int[] arreglo = {5, 12, 9, 20, 8, 15, 2, 17};
            
            int maximo = arreglo[0];
            System.out.println("Arreglo incial:\n");
            imprimirMatriz(arreglo);
            for (int i = 1; i < arreglo.length; i++) {
                if (arreglo[i] > maximo) {
                    maximo = arreglo[i];
                }
            }
            
            System.out.println("\nEl elemento máximo en el arreglo es:\n\n" + maximo);

            
        }

        public static void imprimirMatriz(int[] matriz) {
            int filas = matriz.length;
    
            for (int i = 0; i < filas; i++) {
                    System.out.print(matriz[i] + " ");
                }
                System.out.println();
            }
        }
