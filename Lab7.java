import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P7_123 {
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num1, num2;
        String leer;
        try {
        System.out.println("Introducir los numeros que se quieren sumar (no introducir numeros negativos)");
        
            leer = br.readLine();
            num1 = Integer.parseInt(leer);

            leer = br.readLine();
            num2 = Integer.parseInt(leer);

            if(num1 < 0 || num2 < 0){
                throw new ArithmeticException("Numero debe ser positivo (+)");
            }
            else{
            int suma = num1 + num2;
            System.out.println("El resultado de la suma es: "+suma);
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e){
            System.err.println("Error, porfavor introduzca un numero.");
        }
    }
}
