package Programacion1;

import java.util.Scanner;

public class Practica4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Programa que se encarga de mostrar la cantidad de dias vividos de una persona.
		System.out.println("Problema 4 ");
		
		String nombre;
		int	dia;
		String	mes;
		int	año_de_nacimiento;
		int	dias_vividos;
		int año_actual;
		Scanner sca= new Scanner(System.in);
		System.out.println("Introduce el nombre de la persona: ");
		nombre = sca.next();
		System.out.println("Introduce el dia de nacimiento: ");
		dia=sca.nextInt();
		System.out.println("Introduce el mes de nacimiento: ");
		mes= sca.next();
		System.out.println("Introduce el año de nacimiento: ");
		año_de_nacimiento = sca.nextInt();
		año_actual = 2023;
		dias_vividos= (año_actual - año_de_nacimiento)*365;
		
		System.out.println("la persona conocida	como "+nombre+" nacida el "+dia+" de "+mes+" de "+año_de_nacimiento+", tiene de vivido: "+ dias_vividos);				
	}

}
