package Programacion1;

import java.util.Scanner;

public class Practica2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// leer dos numeros y encontrar la suma del doble del primero mas el cuadrado del segundo
				
				System.out.println("Problema 2: ");
				int num1;
				int num2;
				int proceso;
				Scanner sca= new Scanner(System.in);
				System.out.println("Introduce el primer numero: ");
				num1= sca.nextInt();
				System.out.println("Introduce el segundo numero: ");
				num2= sca.nextInt();
				proceso= num1*2 + num2 * num2;
				System.out.println("La suma del doble del primer numero, con el cuadrado del segundo: " + proceso);
	}

}
