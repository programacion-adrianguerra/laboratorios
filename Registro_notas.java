
package Registro_nota;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Notas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
        Map<String, List<Double>> registroNotas = new HashMap<>();

        while (true) {
            System.out.println("\nMenú:");
            System.out.println("1. Agregar notas");
            System.out.println("2. Mostrar resumen de notas");
            System.out.println("3. Salir");
            System.out.print("Seleccione una opción (1/2/3): ");
            String opcion = scanner.nextLine();

            switch (opcion) {
                case "1":
                    agregarNotas(registroNotas, scanner);
                    break;
                case "2":
                    mostrarResumen(registroNotas);
                    break;
                case "3":
                    System.out.println("¡Hasta luego!");
                    scanner.close();
                    System.exit(0);
                default:
                    System.out.println("Opción no válida. Por favor, elija una opción válida.");
            }
        }
    }

    private static void agregarNotas(Map<String, List<Double>> registroNotas, Scanner scanner) {
        System.out.print("Ingrese el nombre del estudiante: ");
        String nombre = scanner.nextLine();
        List<Double> notas = new ArrayList<>();

        while (true) {
            System.out.print("Ingrese una nota (o escriba 'fin' para terminar): ");
            String notaStr = scanner.nextLine();

            if (notaStr.equalsIgnoreCase("fin")) {
                break;
            }

            try {
                double nota = Double.parseDouble(notaStr);
                notas.add(nota);
            } catch (NumberFormatException e) {
                System.out.println("Ingrese una nota válida.");
            }
        }

        registroNotas.put(nombre, notas);
    }

    private static void mostrarResumen(Map<String, List<Double>> registroNotas) {
        System.out.println("\nResumen de Notas:");
        for (Map.Entry<String, List<Double>> entry : registroNotas.entrySet()) {
            String nombre = entry.getKey();
            List<Double> notas = entry.getValue();
            double promedio = calcularPromedio(notas);

            System.out.print(nombre + ": Notas: " + notas + ", Promedio: ");
            if (notas.isEmpty()) {
                System.out.println("No se ingresaron notas.");
            } else {
                System.out.printf("%.2f%n", promedio);
            }
        }
    }

    private static double calcularPromedio(List<Double> notas) {
        if (notas.isEmpty()) {
            return 0.0;
        }

        double suma = 0;
        for (double nota : notas) {
            suma += nota;
        }

        return suma / notas.size();
    
	}

}
