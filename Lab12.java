import java.util.Scanner;

public class L12_416 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa el primer número: ");
        double num1 = scanner.nextDouble();
        System.out.print("Ingresa el segundo número: ");
        double num2 = scanner.nextDouble();
        System.out.print("Ingresa el tercer número: ");
        double num3 = scanner.nextDouble();


        double maxNumber = Math.max(num1, Math.max(num2, num3));

        System.out.println("El número más grande es: " + maxNumber);

        scanner.close();
    }
}