package Registro_nota;
import java.util.Scanner;
public class Ajedrez {
	public static void main(String[] args) {
        Tablero tablero = new Tablero();

        while (true) {
            tablero.imprimirTablero();
            Scanner scanner = new Scanner(System.in);
            System.out.print("Ingrese la posición de la pieza a mover (fila, columna): ");
            String origenStr = scanner.nextLine();
            System.out.print("Ingrese la posición a la que desea mover la pieza (fila, columna): ");
            String destinoStr = scanner.nextLine();

            String[] origenSplit = origenStr.split(",");
            String[] destinoSplit = destinoStr.split(",");

            int[] origen = { Integer.parseInt(origenSplit[0]), Integer.parseInt(origenSplit[1]) };
            int[] destino = { Integer.parseInt(destinoSplit[0]), Integer.parseInt(destinoSplit[1]) };

            if (tablero.moverPieza(origen, destino)) {
                System.out.println("Movimiento válido.");
            } else {
                System.out.println("Movimiento inválido. Intente nuevamente.");
            }
        }
    }
}

class Pieza {
    protected String color;
    protected String icono;

    public Pieza(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return icono;
    }
}
class Torre extends Pieza {
    public Torre(String color) {
        super(color);
        icono = (color.equals("negro")) ? "♜" : "♖";
    }
}
class Dama extends Pieza {
    public Dama(String color) {
        super(color);
        icono = (color.equals("negro")) ? "♛" : "♕";
    }

    public boolean puedeMover(int[] origen, int[] destino) {
        // Implementa las reglas de movimiento de la dama
        return false;
    }
}

class Rey extends Pieza {
    public Rey(String color) {
        super(color);
        icono = (color.equals("negro")) ? "♚" : "♔";
    }

    public boolean puedeMover(int[] origen, int[] destino) {
        // Implementa las reglas de movimiento del rey
        return false;
    }
}

class Caballo extends Pieza {
    public Caballo(String color) {
        super(color);
        icono = (color.equals("negro")) ? "♞" : "♘";
    }

    public boolean puedeMover(int[] origen, int[] destino) {
        // Implementa las reglas de movimiento del caballo
        return false;
    }
}

class Alfil extends Pieza {
    public Alfil(String color) {
        super(color);
        icono = (color.equals("negro")) ? "♝" : "♗";
    }

    public boolean puedeMover(int[] origen, int[] destino) {
        // Implementa las reglas de movimiento del alfil
        return false;
    }
}

class Peon extends Pieza {
    public Peon(String color) {
        super(color);
        icono = (color.equals("negro")) ? "♙" : "♙";
    }

    public boolean puedeMover(int[] origen, int[] destino) {
        int fila_origen = origen[0];
        int columna_origen = origen[1];
        int fila_destino = destino[0];
        int columna_destino = destino[1];

        if (color.equals("blanco")) {
            if (fila_destino == fila_origen - 1 && columna_destino == columna_origen) {
                return true;
            } else if (fila_destino == fila_origen - 2 && fila_origen == 6 && columna_destino == columna_origen) {
                return true;
            } else if (fila_destino == fila_origen - 1 && Math.abs(columna_destino - columna_origen) == 1) {
                return true;
            }
        } else {
            if (fila_destino == fila_origen + 1 && columna_destino == columna_origen) {
                return true;
            } else if (fila_destino == fila_origen + 2 && fila_origen == 1 && columna_destino == columna_origen) {
                return true;
            } else if (fila_destino == fila_origen + 1 && Math.abs(columna_destino - columna_origen) == 1) {
                return true;
            }
        }

        return false;
    }
}

class Tablero {
    private Pieza[][] tablero; // Matriz para representar el tablero de ajedrez

    public Tablero() {
        tablero = new Pieza[8][8];
        inicializarTablero();
    }

    private void inicializarTablero() {
        for (int i = 0; i < 8; i++) {
            tablero[1][i] = new Peon("negro");
            tablero[6][i] = new Peon("blanco");
        }

        // Torres
        tablero[0][0] = new Torre("negro");
        tablero[0][7] = new Torre("negro");
        tablero[7][0] = new Torre("blanco");
        tablero[7][7] = new Torre("blanco");

        // Caballos
        tablero[0][1] = new Caballo("negro");
        tablero[0][6] = new Caballo("negro");
        tablero[7][1] = new Caballo("blanco");
        tablero[7][6] = new Caballo("blanco");

        // Alfiles
        tablero[0][2] = new Alfil("negro");
        tablero[0][5] = new Alfil("negro");
        tablero[7][2] = new Alfil("blanco");
        tablero[7][5] = new Alfil("blanco");

        // Reyes
        tablero[0][3] = new Rey("negro");
        tablero[7][3] = new Rey("blanco");

        // Damas
        tablero[0][4] = new Dama("negro");
        tablero[7][4] = new Dama("blanco");
    }

    public void imprimirTablero() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(tablero[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean moverPieza(int[] origen, int[] destino) {
        // Implementa la lógica para mover la pieza en el tablero
        return false;
    }
	}
