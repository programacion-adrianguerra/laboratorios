 import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
public class E8_411 {
     public static void main(String[] args) {
     // Crear un nuevo JFrame (ventana) para el formulario
     JFrame frame = new JFrame("Ejemplo de Formulario");
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     frame.setSize(300, 200);
     // Crear un JPanel (panel) para colocar los elementos del formulario
     JPanel panel = new JPanel();
     // Crear un JTextField (campo de texto) y un JButton (botón)
     JTextField textField = new JTextField(20);
     JButton button = new JButton("Enviar");
     // Agregar un ActionListener al botón para manejar eventos de clic
     button.addActionListener(new ActionListener() {
     @Override
     public void actionPerformed(ActionEvent e) {
     // Manejar la acción del botón aquí
     String texto = textField.getText();
     JOptionPane.showMessageDialog(null, "Texto ingresado: " + 
    texto);
     }
     });
     // Agregar los componentes al panel
     panel.add(textField);
     panel.add(button);
     // Agregar el panel al frame
     frame.add(panel);
     // Hacer visible la ventana
     frame.setVisible(true);
     } 
}
