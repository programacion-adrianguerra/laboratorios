package Registro_nota;

public class Galleta {
    private String sabor;
    private String tamaño;
    private String estado;

    public Galleta(String sabor, String tamaño) {
        this.sabor = sabor;
        this.tamaño = tamaño;
        this.estado = "sin morder";
    }

    public void morder() {
        if (estado.equals("sin morder")) {
            estado = "mordida";
            System.out.println("Has mordido la galleta.");
        } else {
            System.out.println("La galleta ya está mordida.");
        }
    }

    public void sumergirEnLeche() {
        if (estado.equals("sin morder")) {
            System.out.println("No puedes sumergir la galleta en leche sin morderla primero.");
        } else {
            estado = "saturada de leche";
            System.out.println("Has sumergido la galleta en leche.");
        }
    }

    public void comer() {
        if (estado.equals("sin morder")) {
            System.out.println("No puedes comer la galleta sin morderla primero.");
        } else if (estado.equals("mordida")) {
            System.out.println("Primero debes sumergir la galleta en leche.");
        } else {
            System.out.println("¡Delicioso! Has comido una galleta de sabor " + sabor + " y tamaño " + tamaño + ".");
            estado = "comida";
        }
    }

    public static void main(String[] args) {
        Galleta miGalleta = new Galleta("chocolate", "pequeña");
        miGalleta.morder();
        miGalleta.sumergirEnLeche();
        miGalleta.comer();
    }
}
