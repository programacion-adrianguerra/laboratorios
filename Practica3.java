package Programacion1;

import java.util.Scanner;

public class Practica3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//lee la cantidad en pies y conviertalas en yardas, pulgadas, metros y centimetros.
		System.out.println("Problema 3: ");
		double num;
		double yardas;
		double pulgadas;
		double centimetros;
		double metros;
		
		System.out.println("Introduce la unidad metrica,conocida como pies para la conversion: ");
		Scanner sca= new Scanner(System.in);
		num= sca.nextInt();
		yardas= num/3;
		pulgadas= num*12;
		centimetros= num * 30.48;
		metros=num* 0.3048;
		System.out.println("Conversión de pies a yardas: " + yardas);
		System.out.println("Conversión de pies a pulgadas: " + pulgadas);
		System.out.println("Conversión de pies a centimetros: " + centimetros);
		System.out.println("Conversión de pies a metros: " + metros);
	}

}
