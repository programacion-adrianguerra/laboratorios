package com.mycompany;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class parcial3 {
    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            // Paso 1: Cargar el controlador JDBC de SQLite
            Class.forName("org.sqlite.JDBC");

            // Paso 2: Establecer la conexión con la base de datos
            connection = DriverManager.getConnection("jdbc:sqlite:mi_base_de_datos.db");

            // Paso 3: Crear una sentencia SQL
            statement = connection.createStatement();

            // Paso 4: Ejecutar una consulta SQL
            resultSet = statement.executeQuery("SELECT * FROM Personas");

            // Paso 5: Procesar los resultados
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String nombre = resultSet.getString("nombre");
                int edad = resultSet.getInt("edad");
                System.out.println("ID: " + id + ", Nombre: " + nombre + ", Edad: " + edad);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Paso 6: Cerrar los recursos
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
